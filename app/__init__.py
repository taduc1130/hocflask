from flask import Flask

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True  # (Thêm dòng này để tự động tải lại mẫu khi có thay đổi)

from app import routes