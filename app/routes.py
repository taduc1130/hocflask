# app/routes.py
from flask import render_template
from app import app

@app.route('/')
@app.route('/home')
def home():
    user = {'username': 'John Doe', 'age': 28}
    return render_template('index.html', user=user)

@app.route('/about')
def about():
    return render_template('about.html')
